
import java.util.Scanner;
public class BI {
	private static Scanner entrada;
	
	public static void main(String[] args) {
		
		entrada = new Scanner(System.in);
		
		System.out.println();
		System.out.println("Programa para determinar nome e idade da pessoa mais nova de um grupo.");
		System.out.println();
		
		int idadeMin = 150;
		for (int i=1 ; i <= 100 ; i++) {
			
			System.out.println("Numero de BI/CC ?");	
			int numero = entrada.nextInt();
			if (numero == 0) {
				System.out.println("A pessoa mais jovem tem "+idadeMin+" anos");
				System.exit(0);
			}			
			System.out.println("Introduza a sua idade");
			int idade = entrada.nextInt();
			if (idade < idadeMin) {
				idadeMin = idade;
			}
		}	
	}
}


