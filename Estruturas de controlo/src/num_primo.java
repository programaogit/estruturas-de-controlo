import java.util.Scanner;

public class num_primo {
	
	private static Scanner entrada;
	
	public static void main(String[] args) {
		
		
		int numero_inteiro;
		entrada = new Scanner(System.in);
		System.out.println();
		System.out.println("Programa para determinar se um n�mero � primo");
		System.out.println();
		
		 
			System.out.println("N�mero?");
			numero_inteiro = entrada.nextInt();
			
			if (numero_inteiro==-1) System.exit(1);
			
			if (numero_inteiro == 1 || numero_inteiro == 0) {
				System.out.println("O n�mero nao � primo");	
				
			}
			
			int divisao=0;
			
			/*
			 * 20%2=0 || 20%5=0 nao e primo
			 * 7%1=0; 7%7=0 primo
			 */
			
			for (int i=2; i < numero_inteiro; i++) {			
				if (numero_inteiro % i == 0 ){
					divisao++;
				}
			}	
			if (divisao > 0 ) {
				System.out.println("O numero" + " " +  numero_inteiro + "  " + "nao � primo");
				
			}else {
				System.out.println("O numero" + " " +  numero_inteiro + "  " + "� primo");		
				
			
			}
		}
	}
