import java.util.Scanner;
public class Notas_Alunos {

	private static Scanner entrada;

	public static void main(String[] args) {
		
		entrada = new Scanner(System.in);	
		
		System.out.println();
		System.out.println("Programa para ler as notas de um aluno �s disciplinas de Matem�tica, Portugu�s, Ingl�s e Geografia e calcular a m�dia");
		System.out.println();
		
			int matematica;
	    //	System.out.println("Nota de Matem�tica? (0 a 20)");
	    //	matematica = entrada.nextInt();
	    	
	    	do {
	    	//	System.out.println("Valor inv�lido");
	    		System.out.println("Introduza a sua nota a Matematica");
		    	matematica = entrada.nextInt();
	    	}while(matematica <0 || matematica > 20);
	    
	    	int port;
	    	System.out.println("Nota de Portugu�s? (0 a 20)");
	    	port = entrada.nextInt();
	    	while (port <0 || port > 20) {
	    		System.out.println("Valor inv�lido");
	    		System.out.println("Nota de Portugu�s? (0 a 20)");
		    	port = entrada.nextInt();
	    	}
	    
	    	int ingles;
	    	System.out.println("Nota de Ingl�s? (0 a 20)");
	    	ingles = entrada.nextInt();
	    	while (ingles <0 || ingles > 20) {
	    		System.out.println("Valor inv�lido");
	    		System.out.println("Nota de Ingl�s? (0 a 20)");
		    	ingles = entrada.nextInt();
	    	}
	    
	    	int geografia;
	    	System.out.println("Nota de Geografia? (0 a 20)");
	    	geografia = entrada.nextInt();
	    	while (geografia <0 || geografia > 20) {
	    		System.out.println("Valor inv�lido");
	    		System.out.println("Nota de Geografia? (0 a 20)");
		    	geografia = entrada.nextInt();
	    	}
	    	
	    	int media;
	    	media = (matematica + port + ingles + geografia) / 4 ;
	    	System.out.println("O aluno" + " " + "teve uma media de" + " " + media);
			
	    	if (media>= 9.5)
				System.out.println("Aprovado");
				
			else
				System.out.println("Reprovado");
	    
	}

}